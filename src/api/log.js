import axios from "axios";

const client = axios.create({
  baseURL: "http://localhost:8080/identiticoders-jersey-api/",
  json: true
})

export default {
  async execute(method, resource, data) {
    return client({
      method,
      url: resource,
      data
    }).then((req) => {
      return req.data;
    });
  },
  getAllData() {
    return this.execute("get", "/logs");
  }
};
