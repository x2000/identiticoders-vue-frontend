import axios from "axios";

const client = axios.create({
  baseURL: "http://localhost:8080/identiticoders-jersey-api/",
  json: true
})

export default {
  async execute(method, resource, data) {
    return client({
      method,
      url: resource,
      data
    }).then((resp) => {
      // console.log(resp.data.access_token)
      const token = resp.data.access_token
      localStorage.setItem('user-token', token) 
      return resp.data;
    });
  },
  doLogin(data) {
    return this.execute("POST", "/authenticate", data);
  }
};
