import axios from "axios";

const client = axios.create({
  baseURL: "http://localhost:8080/identiticoders-jersey-api",
  json: true
})

export default {
  async execute(method, resource, data, headers) {
    return client({
      method,
      url: resource,
      headers,
      data
    }).then((req) => {
      return req.data;
    });
  },
  getAllData() {
    const token = localStorage.getItem('user-token');
    const headers = {'access_token': token}
    return this.execute("get", "/users", null, headers);
  },
  getSingleData(id) {
    return this.execute("get", `/users/${id}`);
  },
  createData(data) {
    const token = localStorage.getItem('user-token');
    const headers = {'access_token': token}
    return this.execute("post", "/users", data, headers);
  },
  updateData(id, data) {
    const token = localStorage.getItem('user-token');
    const headers = {'access_token': token}
    return this.execute("put", `/users/${id}`, data, headers);
  },
  deleteData(id) {
    const token = localStorage.getItem('user-token');
    const headers = {'access_token': token}
    return this.execute("delete", `/users/${id}`, null, headers);
  }
};
